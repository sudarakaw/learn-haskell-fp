module Data
  ( Maybe(..)
  , Tree(..)
  , Expr(..)
  , ST(..)
  , appST
  , State
  ) where

import           Prelude                 hiding ( Maybe(..) )

data Maybe a = Nothing | Just a
  deriving Show


data Tree a = Leaf a | Node (Tree a) (Tree a)
  deriving Show


data Expr = Val Int | Div Expr Expr
  deriving Show


type State = Int

newtype ST a = S (State -> (a, State))

appST :: ST a -> State -> (a, State)
appST (S st) s = st s
