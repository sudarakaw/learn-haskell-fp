import           Data.List

boxsize :: Int
boxsize = 3

-- MODEL

type Grid = Matrix Value

type Value = Char

type Matrix a = [Row a]

type Row a = [a]

type Choices = [Value]

-- Example Grids

easy :: Grid
easy =
  [ "2....1.38"
  , "........5"
  , ".7...6..."
  , ".......13"
  , ".981..257"
  , "31....8.."
  , "9..8...2."
  , ".5..69784"
  , "4..25...."
  ]

gentle :: Grid
gentle =
  [ ".1.42...5"
  , "..2.71.39"
  , ".......4."
  , "2.71....6"
  , "....4...."
  , "6....74.3"
  , ".7......."
  , "12.73.5.."
  , "3...82.7."
  ]

diabolical :: Grid
diabolical =
  [ ".9.7..86."
  , ".31..5.2."
  , "8.6......"
  , "..7.5...6"
  , "...3.7..."
  , "5...1.7.."
  , "......1.9"
  , ".2.6..35."
  , ".54..8.7."
  ]

unsolvable :: Grid
unsolvable =
  [ "1..9.7..3"
  , ".8.....7."
  , "..9...6.."
  , "..72.94.."
  , "41.....95"
  , "..85.43.."
  , "..3...7.."
  , ".5.....4."
  , "2..8.6..9"
  ]

minimal :: Grid
minimal =
  [ ".98......"
  , "....7...."
  , "....15..."
  , "1........"
  , "...2....9"
  , "...9.6.82"
  , ".......3."
  , "5.1......"
  , "...4...2."
  ]

blank :: Int -> Grid
blank boxsize = replicate n (replicate n '.') where n = boxsize ^ 2


rows :: Matrix a -> [Row a]
-- Note:
--  Property : rows ( rows m ) = m
--             rows . rows     = id
-- rows m = m
rows = id

cols :: Matrix a -> [Row a]
-- Note:
--  Property : cols ( cols m ) = m
--             cols . cols     = id
cols = transpose

boxes :: Matrix a -> [Row a]
-- Note:
--  Property : boxes ( boxes m ) = m
--             boxes . boxes     = id
boxes = unpack . map cols . pack where
  pack   = split . map split
  split  = chop boxsize
  unpack = map concat . concat

chop :: Int -> [a] -> [[a]]
chop _ [] = []
chop n xs = take n xs : chop n (drop n xs)

valid :: Grid -> Bool
valid g = all nodups (rows g) && all nodups (cols g) && all nodups (boxes g)

nodups :: Eq a => [a] -> Bool
nodups []       = True
nodups (x : xs) = x `notElem` xs && nodups xs


-- SOLVERS

solve :: Grid -> [Grid]
-- NOTE: this will take long time to calculate
--       `easy` grid will involve over 9^51 calculations
solve = filter valid . collapse . choices

choices :: Grid -> Matrix Choices
choices = map (map choice)
  where choice v = if v == '.' then ['1' .. '9'] else [v]

collapse :: Matrix [a] -> [Matrix a]
collapse m = cp (map cp m)

cp :: [[a]] -> [[a]]
cp []         = [[]]
cp (xs : xss) = [ y : ys | y <- xs, ys <- cp xss ]


prune :: Matrix Choices -> Matrix Choices
prune = pruneBy boxes . pruneBy cols . pruneBy rows
  where pruneBy f = f . map reduce . f

reduce :: Row Choices -> Row Choices
reduce css = [ cs `without` singles | cs <- css ]
  where singles = concat (filter single css)

single :: [a] -> Bool
single [_] = True
single _   = False

without :: Choices -> Choices -> Choices
xs `without` ys = if single xs then xs else xs \\ ys


solve2 :: Grid -> [Grid]
-- NOTE: this will *still* take long time to calculate
--       `easy` grid will involve over 10^24 calculations
solve2 = filter valid . collapse . prune . choices

solve3 :: Grid -> [Grid]
-- NOTE: this *WILL* solve the `easy` grig quickly,
--       however still take long time to solve more advanced grids
solve3 = filter valid . collapse . fix prune . choices

fix :: Eq a => (a -> a) -> a -> a
fix f x = if x == x' then x else fix f x' where x' = f x


void :: Matrix Choices -> Bool
void = any $ any null

safe :: Matrix Choices -> Bool
safe m =
  all consistent (rows m) && all consistent (cols m) && all consistent (boxes m)

consistent :: Row Choices -> Bool
-- consistent = nodups . concat . filter single
consistent = nodups . filter single

blocked :: Matrix Choices -> Bool
blocked m = void m || not (safe m)


solve4 :: Grid -> [Grid]
solve4 = search . prune . choices

search :: Matrix Choices -> [Grid]
search m | blocked m          = []
         | all (all single) m = collapse m
         | otherwise          = [ g | m' <- expand m, g <- search (prune m') ]


expand :: Matrix Choices -> [Matrix Choices]
expand m = [ lrs ++ [lr ++ [c] : rr] ++ rrs | c <- cr ]
 where
  (lrs, crs : rrs) = break (any (not . single)) m
  (lr , cr : rr  ) = break (not . single) crs



-- MAIN

main :: IO ()
main = do
  -- print $ solve4 easy
  -- print $ solve4 gentle
  -- print $ solve4 diabolical
  -- print $ solve4 unsolvable
  putStrLn $ unlines $ head $ solve4 minimal
  -- print $ take 10 $ solve4 (blank boxsize)
