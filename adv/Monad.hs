module Monad
  ( Monad
  , pure
  , (>>=)
  ) where

import           Prelude                 hiding ( (>>=)
                                                , Applicative
                                                , Maybe(..)
                                                , Monad
                                                , pure
                                                , return
                                                )

import           Applicative
import           Data                           ( Expr(..)
                                                , Maybe(..)
                                                , ST(..)
                                                , State
                                                , Tree(..)
                                                , appST
                                                )
import           Evaluator                      ( safeDiv )


-- MONAD TYPE CLASS

class Applicative m => Monad m where
  -- bind operator
  (>>=) :: m a -> (a -> m b) -> m b

  return :: a -> m a
  return = pure


-- MAYBE MONAD

instance Monad Maybe where
  -- (>>=) :: Maybe a -> (a -> Maybe b) -> Maybe b
  Nothing >>= _ = Nothing
  Just x  >>= f = f x


-- LIST MONAD

instance Monad [] where
  -- (>>=) :: [a] -> (a -> [b]) -> [b]
  [] >>= _ = []
  xs >>= f = concat $ fmap f xs
  -- other implementations
  -- xs >>= f = concat $ map f xs
  -- xs >>= f = [ y | x <- xs, y <- f x ]


-- STATE MONAD

instance Monad ST where
  -- return :: a -> ST a
  return x = S (\s -> (x, s))

  -- (>>=) :: ST a -> (a -> ST b) -> ST b
  st >>= f = S (\s -> let (x, s') = appST st s in appST (f x) s')



-- OPERATIONS

pairs :: [a] -> [b] -> [(a, b)]
pairs xs ys = do
  x <- xs
  y <- ys
  return (x, y)

mEval :: Expr -> Maybe Int
mEval (Val n  ) = Just n
mEval (Div x y) = mEval x >>= (\n -> mEval y >>= (\m -> safeDiv n m))

rlabel :: Tree a -> Int -> (Tree Int, Int)
rlabel (Leaf x  ) n = (Leaf n, n + 1)
rlabel (Node l r) n = (Node l' r', n'')
 where
  (l', n' ) = rlabel l n
  (r', n'') = rlabel r n'

fresh :: ST Int
fresh = S (\n -> (n, n + 1))

mlabel :: Tree a -> ST (Tree Int)
mlabel (Leaf x) = fresh >>= (\n -> return $ Leaf n)
mlabel (Node l r) =
  mlabel l >>= (\l' -> mlabel r >>= (\r' -> return (Node l' r')))

label :: Tree a -> Tree Int
label t = fst $ appST (mlabel t) 0


-- OUTPUT
main :: IO ()
main = do
  -- putStrLn "Maybe Monad"
  -- putStrLn $ "42 / 7 : " ++ show (bindEval goodExpr)
  -- putStrLn $ "42 / 0 : " ++ show (bindEval badExpr)

  putStrLn ""
  putStrLn "List Monad"
  putStrLn $ "pairs [1 ,2] [3, 4] : " ++ show (pairs [1, 2] [3, 4])

  putStrLn ""
  putStrLn "Tree Monad"

  let t = Node (Node (Leaf 'a') (Leaf 'b')) (Leaf 'c')

  putStrLn $ "Retag tree with rlabel : " ++ show (rlabel t 1)
  putStrLn $ "Retag tree with mlabel : " ++ show (appST (mlabel t) 1)
  putStrLn $ "Retag tree with label  : " ++ show (label t)
