module Applicative
  ( Applicative
  , pure
  ) where

import           Prelude                 hiding ( (<*>)
                                                , Applicative
                                                , Functor
                                                , Maybe(..)
                                                , fmap
                                                , pure
                                                )

import           Data                           ( Maybe(..)
                                                , ST(..)
                                                , appST
                                                )
import           Functor                        ( Functor
                                                , fmap
                                                )


-- EXPAND FUNCTOR

class FunctorX f where
  fmap0 :: a -> f a

  fmap1 :: (a -> b) -> f a  -> f b  -- Regular Functor

  fmap2 :: (a -> b -> c) -> f a  -> f b -> f c

  fmap3 :: (a -> b -> c -> d) -> f a  -> f b -> f c -> f d


instance FunctorX Maybe where
  -- fmap0 :: a -> f a
  fmap0 x = Just x

  -- fmap1 :: (a -> b) -> f a -> f b  -- Regular Functor
  fmap1 = fmap

  -- fmap2 :: (a -> b -> c) -> f a -> f b -> f c
  fmap2 _ Nothing  _        = Nothing
  fmap2 _ _        Nothing  = Nothing
  fmap2 f (Just x) (Just y) = Just (f x y)

  -- fmap3 :: (a -> b -> c -> d) -> f a -> f b -> f c -> f d
  fmap3 _ Nothing  _        _        = Nothing
  fmap3 _ _        Nothing  _        = Nothing
  fmap3 _ _        _        Nothing  = Nothing
  fmap3 f (Just x) (Just y) (Just z) = Just (f x y z)


-- APPLICATIVE TYPE CLASS

class Functor f => Applicative f where
  pure :: a -> f a

  (<*>) :: f (a -> b) -> f a -> f b


-- APPLICABLE TYPES

instance  Applicative Maybe where
  -- pure :: a -> f a
  pure = Just

  -- (<*>) :: f (a -> b) -> Maybe b -> Maybe b
  Nothing  <*> _  = Nothing
  (Just f) <*> mx = fmap f mx


instance Applicative [] where
  -- pure :: a -> [a]
  pure x = [x]

  -- (<*>) :: [a -> b] -> [a] -> [b]
  fs <*> xs = [ f x | f <- fs, x <- xs ]


instance Applicative ST where
  -- pure :: a -> ST a
  pure x = S (\s -> (x, s))

  -- (<*>) :: f (a -> b) -> ST a -> ST b
  stf <*> stx = S $ \s ->
    let (f, s1) = appST stf s
        (x, s2) = appST stx s1
    in  (f x, s2)


-- OUTPUT

main :: IO ()
main = do
  putStrLn "Expand Functor"
  putStrLn $ "Just 3 * 2  : " ++ show (fmap1 (* 2) $ Just 3)
  putStrLn $ "Just 3 * Just 2  : " ++ show (fmap2 (*) (Just 3) (Just 2))

  putStrLn ""
  putStrLn "Applicable Types"
  putStrLn $ "(Just (* 2)) (Just 3)       : " ++ show (pure (* 2) <*> Just 3)
  putStrLn $ "(Just *) (Just 3) (Just 2)  : " ++ show
    (pure (*) <*> Just 3 <*> Just 2)
  putStrLn $ "(Just +) Nothing (Just 2)   : " ++ show
    (pure (*) <*> Nothing <*> Just 2)
  putStrLn $ "(Just *) (Just 3) Nothing   : " ++ show
    (pure (*) <*> Just 3 <*> Nothing)

  putStrLn ""
  putStrLn $ "[(* 2)] [1, 2, 3]   : " ++ show (pure (* 2) <*> [1, 2, 3])
  putStrLn $ "[(+)] [2, 3] [4, 5] : " ++ show (pure (+) <*> [2, 3] <*> [4, 5])
