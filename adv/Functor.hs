module Functor
  ( fmap
  , Functor
  ) where

import           Prelude                 hiding ( Functor
                                                , Maybe(..)
                                                , fmap
                                                , map
                                                )


import           Data

-- MAPPABLE LISTS

inc :: [Int] -> [Int]
inc []       = []
inc (n : ns) = n + 1 : inc ns


sqr :: [Int] -> [Int]
sqr []       = []
sqr (n : ns) = n ^ 2 : sqr ns


-- MAPPED
map :: (a -> b) -> [a] -> [b]
map _ []       = []
map f (x : xs) = f x : map f xs

minc :: [Int] -> [Int]
minc = map (+ 1)

msqr :: [Int] -> [Int]
msqr = map (^ 2)


-- MAPPABLE MAYBE

maybeMap :: (a -> b) -> Maybe a -> Maybe b
maybeMap _ Nothing  = Nothing
maybeMap f (Just x) = Just $ f x


-- FUNCTOR TYPE CLASS

class Functor f where
  fmap :: (a -> b) -> f a -> f b


-- FUNCTORIZED TYPES

instance Functor Maybe where
  -- fmap :: (a -> b) -> Maybe a -> Maybe b
  fmap _ Nothing  = Nothing
  fmap f (Just x) = Just $ f x


instance Functor Tree where
  -- fmap :: (a -> b) -> Tree -> Tree b
  fmap f (Leaf x  ) = Leaf $ f x
  fmap f (Node l r) = Node (fmap f l) (fmap f r)


instance Functor [] where
  -- fmap :: (a -> b) -> [a] -> [b]
  fmap = map


instance Functor ST where
  -- fmap :: (a -> b) -> ST a -> ST b
  fmap f st = S $ \s -> let (x, s') = appST st s in (f x, s')


-- FUNCTORIZED OPERATIONS

finc :: Functor f => f Int -> f Int
finc = fmap (+ 1)


fsqr :: Functor f => f Int -> f Int
fsqr = fmap (^ 2)


-- OUTPUT

main :: IO ()
main = do
  putStrLn "Mappable Lists"
  putStrLn $ "inc : " ++ show (inc [1, 2, 3])
  putStrLn $ "sqr : " ++ show (sqr [1, 2, 3])

  putStrLn ""
  putStrLn "Mapped"
  putStrLn $ "minc : " ++ show (minc [1, 2, 3])
  putStrLn $ "msqr : " ++ show (msqr [1, 2, 3])

  putStrLn ""
  putStrLn "Mappable Maybe"
  putStrLn $ "Nothing + 1 : " ++ show (maybeMap (+ 1) Nothing)
  putStrLn $ "Just 2 + 1  : " ++ show (maybeMap (+ 1) $ Just 2)

  putStrLn ""
  putStrLn "Maybe Functor"
  putStrLn $ "Nothing * 2 : " ++ show (fmap (* 2) Nothing)
  putStrLn $ "Just 3 * 2  : " ++ show (fmap (* 2) $ Just 3)

  putStrLn ""
  putStrLn "Tree Functor"
  putStrLn $ "length of Leaf \"abc\"           : " ++ show
    (fmap length (Leaf "abc"))
  putStrLn $ "Is Tree (Leaf 1) (Leaf 2) even : " ++ show
    (fmap even (Node (Leaf 1) (Leaf 2)))

  putStrLn ""
  putStrLn "Functiozed Opetations"
  putStrLn $ "finc [1, 2, 3]               : " ++ show (finc [1, 2, 3])
  putStrLn $ "fsqr [1, 2, 3]               : " ++ show (fsqr [1, 2, 3])
  putStrLn $ "finc Just 3                  : " ++ show (finc $ Just 3)
  putStrLn $ "fsqr Just 3                  : " ++ show (fsqr $ Just 3)
  putStrLn $ "finc Nothing                 : " ++ show (finc Nothing)
  putStrLn $ "fsqr Nothing                 : " ++ show (fsqr Nothing)
  putStrLn $ "finc Node (Leaf 5) (Leaf 11) : " ++ show
    (finc (Node (Leaf 5) (Leaf 11)))
  putStrLn $ "fsqr Node (Leaf 5) (Leaf 11) : " ++ show
    (fsqr (Node (Leaf 5) (Leaf 11)))
