-- MODEL

data Expr a = Var a
            | Val Int
            | Add (Expr a) (Expr a)
            deriving Show

instance Functor Expr where
  -- fmap :: (a -> b) -> Expr a -> Expr b
  fmap f (Var x    ) = Var $ f x
  fmap _ (Val x    ) = Val x
  fmap f (Add el er) = Add (fmap f el) (fmap f er)

instance Applicative Expr where
  -- pure :: a -> Expr a
  pure = Var

  -- (<*>) :: Expr (a -> b) -> Expr a -> Expr b
  (Var f    ) <*> expr = fmap f expr
  (Val n    ) <*> _    = Val n
  (Add el er) <*> expr = Add (el <*> expr) (er <*> expr)

instance Monad Expr where
  -- return :: a -> Expr a
  return = Var

  -- (>>=) :: Expr a -> (a -> Expr b) -> Expr b
  (Var x    ) >>= f = f x
  (Val x    ) >>= _ = Val x
  (Add el er) >>= f = Add (el >>= f) (er >>= f)


-- MAIN

main :: IO ()
main = do
  print "Ok"
