module Generic
  () where

import           Data.Char
import           Prelude                 hiding ( Maybe(..)
                                                , Monad(..)
                                                , mapM
                                                , pure
                                                , return
                                                )

import           Applicative
import           Data
import           Monad

mapM :: Monad m => (a -> m b) -> [a] -> m [b]
mapM f [] = pure []
mapM f (x : xs) =
  -- do
  -- y  <- f x
  -- ys <- mapM f xs
  -- pure $ y : ys
  f x >>= (\y -> mapM f xs >>= (\ys -> pure $ y : ys))


conv :: Char -> Maybe Int
conv c | isDigit c = Just $ digitToInt c
       | otherwise = Nothing

concat :: [[a]] -> [a]
concat xss = [ x | xs <- xss, x <- xs ]

join :: Monad m => m (m a) -> m a
join mmx =
  -- do
  -- mx <- mmx
  -- x  <- mx
  -- pure $ x
  mmx >>= (\mx -> mx >>= (\x -> pure x))

main :: IO ()
main = do
  putStrLn "Digit Conversion"
  putStrLn $ "Convert string \"1234\" : " ++ show (mapM conv "1234")
  putStrLn $ "Convert string \"12x4\" : " ++ show (mapM conv "12x4")

  putStrLn ""
  putStrLn "Join/Flatten Nested Monad"
  putStrLn $ "Join [[1, 2], [3, 4], [5, 6]] : " ++ show
    (join [[1, 2], [3, 4], [5, 6]])
  putStrLn $ "Join Just (Just 1))           : " ++ show (join (Just (Just 1)))
