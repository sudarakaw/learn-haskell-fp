module Evaluator
  ( safeDiv
  ) where

import           Prelude                 hiding ( Maybe(..)
                                                , Monad
                                                )

import           Data                           ( Expr(..)
                                                , Maybe(..)
                                                )


-- OPERATIONS

-- unsafe
unsafeEval :: Expr -> Int
unsafeEval (Val n    ) = n
unsafeEval (Div e1 e2) = unsafeEval e1 `div` unsafeEval e2


-- safe helper
safeDiv :: Int -> Int -> Maybe Int
safeDiv _ 0 = Nothing
safeDiv x y = Just $ x `div` y

-- safe
safeEval :: Expr -> Maybe Int
safeEval (Val n    ) = Just n
safeEval (Div e1 e2) = case safeEval e1 of
  Nothing -> Nothing
  Just x  -> case safeEval e2 of
    Nothing -> Nothing
    Just y  -> safeDiv x y


-- safe applictive ?
-- apEval :: Expr -> Maybe Int
-- apEval (Val n    ) = pure n
-- apEval (Div e1 e2) = pure safeDiv <*> apEval e1 <*> apEval e2

--
-- Issue with applicative approach.
--
-- Using applicative doesn't type check because, first pure call expects
-- Int -> Int -> Int, and safeDiv's type is is Int -> Int -> Maybe Int
-- Common pattern in the safe eval function above is as follows:
--
--   case *mx* of
--     Nothing -> Nothing
--     Just x  -> *f* x
--
-- Therefor:
--    mx `op` f = case mx of
--                  Nothing -> Nothing
--                  Just x  -> f x
--
-- Type of the "op" is:
--    (op) :: Maybe a -> (a -> Maybe b) -> Maybe b
--
-- ** This operator is called "BIND".
--

bind :: Maybe a -> (a -> Maybe b) -> Maybe b
Nothing  `bind` _ = Nothing
(Just x) `bind` f = f x

-- using bind
bindEval :: Expr -> Maybe Int
bindEval (Val n) = Just n
bindEval (Div x y) =
  bindEval x `bind` (\n -> bindEval y `bind` (\m -> safeDiv n m))

--
-- Common pattern in using bind is as follows:
--
--    m1 `bind` \x1 -> ...
--    m2 `bind` \x2 -> ...
--    .
--    .
--    mn `bind` \xn -> ...
--    f x1 x2 ... xn
--
-- This can be simplify by do notation
--
--    do x1 <- m1
--       x2 <- m2
--       .
--       .
--       xn <- mn
--       f x1 x2 ... xn
--

-- using bind with do notation
-- bindDoEval :: Expr -> Maybe Int
-- -- NOTE: this will only work with the Maybe from prelude because do notation
-- -- require Maybe to be implement the Monad type class.
-- bindDoEval (Val n  ) = Just n
-- bindDoEval (Div x y) = do
--   n <- bindDoEval x
--   m <- bindDoEval y
--   safeDiv n m


-- OUTPUT

main :: IO ()
main = do
  let goodExpr = Div (Val 42) (Val 7)
      badExpr  = Div (Val 42) (Val 0)

  putStrLn "Unsafe Eval"
  putStrLn $ "42 / 7 : " ++ show (unsafeEval goodExpr)
  -- putStrLn $ "42 / 0 : " ++ show (unsafeEval badExpr)  -- *** Exception: divide by zero

  putStrLn ""
  putStrLn "Safe Eval"
  putStrLn $ "42 / 7 : " ++ show (safeEval goodExpr)
  putStrLn $ "42 / 0 : " ++ show (safeEval badExpr)

  putStrLn ""
  putStrLn "Bind based Eval"
  putStrLn $ "42 / 7 : " ++ show (bindEval goodExpr)
  putStrLn $ "42 / 0 : " ++ show (bindEval badExpr)

  -- putStrLn ""
  -- putStrLn "Bind based Eval with do notation"
  -- putStrLn $ "42 / 7 : " ++ show (bindDoEval goodExpr)
  -- putStrLn $ "42 / 0 : " ++ show (bindDoEval badExpr)
