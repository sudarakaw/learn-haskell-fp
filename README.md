# Functional programming Course

Practice code created while following Functional programming course by [Graham Hutton](http://www.cs.nott.ac.uk/~pszgmh/).

## Code Directories

**1. intro** - [Functional Programming](http://www.cs.nott.ac.uk/~pszgmh/pgp.html)

  [videos](https://www.youtube.com/playlist?list=PLF1Z-APd9zK7usPMx3LGMZEHrECUGodd3)

**2. adv** - [Advanced Functional Programming](http://www.cs.nott.ac.uk/~pszgmh/afp.html)

  [videos](https://www.youtube.com/playlist?list=PLF1Z-APd9zK5uFc8FKr_di9bfsYv8-lbc)
