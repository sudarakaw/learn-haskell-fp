data Op = Add
        | Sub
        | Mul
        | Div

instance Show Op where
  show Add = "+"
  show Sub = "-"
  show Mul = "*"
  show Div = "/"


data Expr = Val Int
          | App Op Expr Expr

instance Show Expr where
  show (Val n    ) = show n
  show (App o l r) = brak l ++ show o ++ brak r
   where
    brak (Val n) = show n
    brak e       = "(" ++ show e ++ ")"


apply :: Op -> Int -> Int -> Int
apply Add x y = x + y
apply Sub x y = x - y
apply Mul x y = x * y
apply Div x y = x `div` y


valid :: Op -> Int -> Int -> Bool
valid Add x y = x <= y
valid Sub x y = x > y
valid Mul x y = x <= y && x /= 1 && y /= 1
valid Div x y = x `mod` y == 0 && y /= 1


eval :: Expr -> [Int]
eval (Val n) = [ n | n > 0 ]
eval (App operator operandL operandR) =
  [ apply operator x y
  | x <- eval operandL
  , y <- eval operandR
  , valid operator x y
  ]

subs :: [a] -> [[a]]
subs []       = [[]]
subs (x : xs) = yss ++ map (x :) yss where yss = subs xs

interleave :: a -> [a] -> [[a]]
interleave x []       = [[x]]
interleave x (y : ys) = (x : y : ys) : map (y :) (interleave x ys)

perms :: [a] -> [[a]]
perms []       = [[]]
perms (x : xs) = concat (map (interleave x) (perms xs))

choices :: [a] -> [[a]]
choices = concat . map perms . subs


values :: Expr -> [Int]
values (Val n        ) = [n]
values (App _ opL opR) = values opL ++ values opR


solution :: Expr -> [Int] -> Int -> Bool
solution exp ns n = elem (values exp) (choices ns) && eval exp == [n]


split :: [a] -> [([a], [a])]
split []       = []
split [_     ] = []
split (x : xs) = ([x], xs) : [ (x : ls, rs) | (ls, rs) <- split xs ]


combine :: Expr -> Expr -> [Expr]
combine l r = [ App op l r | op <- [Add, Sub, Mul, Div] ]


exprs :: [Int] -> [Expr]
exprs []  = []
exprs [n] = [Val n]
exprs ns =
  [ e | (ls, rs) <- split ns, l <- exprs ls, r <- exprs rs, e <- combine l r ]


solutions :: [Int] -> Int -> [Expr]
solutions ns n = [ e | ns' <- choices ns, e <- exprs ns', eval e == [n] ]


-- OPTIMIZATION


type Result = (Expr, Int)


results :: [Int] -> [Result]
-- results ns = [ (e, n) | e <- exprs ns, n <- eval e ]
results []  = []
results [n] = [ (Val n, n) | n > 0 ]
results ns =
  [ res
  | (ls, rs) <- split ns
  , l        <- results ls
  , r        <- results rs
  , res      <- combine' l r
  ]


combine' :: Result -> Result -> [Result]
combine' (l, x) (r, y) =
  [ (App op l r, apply op x y) | op <- [Add, Sub, Mul, Div], valid op x y ]


solutions' :: [Int] -> Int -> [Expr]
solutions' ns n = [ e | ns' <- choices ns, (e, m) <- results ns', m == n ]


-- MAIN


main = do
  -- print $ solutions [1, 3, 7, 10, 25, 50] 765
  -- print $ solutions [1, 3, 7, 10] 56
  print $ solutions' [1, 3, 7, 10, 25, 50] 765
