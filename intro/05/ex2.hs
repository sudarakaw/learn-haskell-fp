factors :: Int -> [Int]
factors n = [ x | x <- [1 .. n], n `mod` x == 0 ]

perfect :: Int -> Bool
perfect n = sum (init (factors n)) == n

perfectTo :: Int -> [Int]
perfectTo n = [ x | x <- [1 .. n], perfect x ]

main = do
  print $ perfect 6
  print $ perfect 28

  print $ perfectTo 500
