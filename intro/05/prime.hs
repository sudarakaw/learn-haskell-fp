factors :: Int -> [Int]
factors n = [ x | x <- [1 .. n], n `mod` x == 0 ]


prime :: Int -> Bool
prime n = factors n == [1, n]

primesTo :: Int -> [Int]
primesTo n = [ x | x <- [2 .. n], prime x ]

main = do
  print $ factors 1
  print $ factors 4
  print $ factors 6
  print $ factors 7
  print $ factors 15

  print $ prime 1
  print $ prime 2
  print $ prime 7
  print $ prime 16
  print $ prime 39

  print $ primesTo 10
  print $ primesTo 50
  print $ primesTo 100
  -- print $ primesTo 5000
