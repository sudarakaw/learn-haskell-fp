sprod' :: [Int] -> [Int] -> Int
sprod' xs ys = sum [ xs !! i * ys !! i | i <- [0 .. n - 1] ]
  where n = length xs

sprod :: [Int] -> [Int] -> Int
sprod xs ys = sum [ x * y | (x, y) <- zip xs ys ]

main = do
  print $ sprod' [1, 2, 3] [2, 3, 4] == 20
  print $ sprod [1, 2, 3] [2, 3, 4] == 20
