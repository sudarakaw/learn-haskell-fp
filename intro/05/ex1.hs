pyth :: Int -> [(Int, Int, Int)]
pyth n =
  [ (x, y, z)
  | x <- [1 .. n]
  , y <- [1 .. n]
  , z <- [1 .. n]
  , x ^ 2 + y ^ 2 == z ^ 2
  ]

main = do
  print $ pyth 5 == [(3, 4, 5), (4, 3, 5)]
  print $ pyth 10 == [(3, 4, 5), (4, 3, 5), (6, 8, 10), (8, 6, 10)]
