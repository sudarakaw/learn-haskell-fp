or1 :: Bool -> Bool -> Bool
or1 True  True  = True
or1 False True  = True
or1 True  False = True
or1 False False = False

or2 :: Bool -> Bool -> Bool
or2 False False = False
or2 _     _     = True

or3 :: Bool -> Bool -> Bool
False `or3` b = b
True  `or3` _ = True
