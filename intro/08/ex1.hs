data Nat = Zero | Succ Nat
  deriving Show

nadd :: Nat -> Nat -> Nat
nadd Zero     n = n
nadd (Succ m) n = Succ (nadd m n)

nmul :: Nat -> Nat -> Nat
nmul Zero     _ = Zero
-- Reasoning:
-- (Succ m) => 1 + m
-- Therefore we have here
--    (1 + m) * n
--  = (1 * n) + (m * n)
--  = n + (m * n)
nmul (Succ m) n = nadd n (nmul m n)

main = do
  print $ nadd Zero Zero
  print $ nadd (Succ Zero) Zero
  print $ nadd Zero (Succ Zero)
  print $ nadd (Succ Zero) (Succ Zero)
  print $ nadd (Succ (Succ Zero)) (Succ Zero)
  print $ nadd (Succ Zero) (Succ (Succ Zero))

  print $ nmul Zero Zero
  print $ nmul (Succ Zero) Zero
  print $ nmul Zero (Succ Zero)
  print $ nmul (Succ Zero) (Succ Zero)
  print $ nmul (Succ (Succ Zero)) (Succ Zero)
  print $ nmul (Succ Zero) (Succ (Succ Zero))
  print $ nmul (Succ (Succ Zero)) (Succ (Succ Zero))
