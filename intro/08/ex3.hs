data Tree a = Leaf a
            | Node (Tree a) (Tree a)
            deriving Show


main = do
  print $ Node (Leaf "A") (Node (Node (Leaf "B") (Leaf "C")) (Leaf "D"))
