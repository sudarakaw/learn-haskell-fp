data Expr = Val Int
          | Add Expr Expr
          | Mul Expr Expr
  deriving Show

eval :: Expr -> Int
eval (Val n    ) = n
eval (Add e1 e2) = eval e1 + eval e2
eval (Mul e1 e2) = eval e1 * eval e2

folde
  :: (Int -> Int) -> (Int -> Int -> Int) -> (Int -> Int -> Int) -> Expr -> Int
folde fVal _ _ (Val x) = fVal x
folde fVal fAdd fMul (Add e1 e2) =
  fAdd (folde fVal fAdd fMul e1) (folde fVal fAdd fMul e2)
folde fVal fAdd fMul (Mul e1 e2) =
  fMul (folde fVal fAdd fMul e1) (folde fVal fAdd fMul e2)

main = do
  print $ eval (Val 5)
  print $ eval (Add (Val 5) (Val 4))
  print $ eval (Mul (Val 5) (Val 4))
  print $ eval (Mul (Add (Val 5) (Val 4)) (Val 4))

  print $ folde id (+) (*) (Val 5)
  print $ folde id (+) (*) (Add (Val 5) (Val 4))
  print $ folde id (+) (*) (Mul (Val 5) (Val 4))
  print $ folde id (+) (*) (Mul (Add (Val 5) (Val 4)) (Val 4))
