merge :: Ord a => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
-- merge (x : xs) (y : ys) | x > y     = y : x : merge xs ys
--                         | otherwise = x : y : merge xs ys
merge (x : xs) (y : ys) | x > y     = y : merge (x : xs) ys
                        | otherwise = x : merge xs (y : ys)

msort :: Ord a => [a] -> [a]
msort []  = []
msort [x] = [x]
msort xs  = merge (msort fh) (msort sh)
  where (fh, sh) = splitAt (length xs `div` 2) xs

main = do
  print $ merge [2, 5, 6] [1, 3, 4]
  print $ merge [1, 3, 4] [2, 5, 6]

  print $ msort [1, 3, 4, 2, 5, 6]
  print $ msort "Sudaraka"
