-- and' :: [Bool] -> Bool
-- and' []           = False
-- and' [True      ] = True
-- and' (False : _ ) = False
-- and' (_     : xs) = and' xs

and' :: [Bool] -> Bool
and' []       = True
-- and' [x         ] = x
-- and' (False : _ ) = False
-- and' (_     : xs) = and' xs
and' (x : xs) = x && and' xs

flatten :: [[a]] -> [a]
flatten []         = []
flatten (xs : xss) = xs ++ flatten xss

replicate' :: Int -> a -> [a]
replicate' 0 _ = []
replicate' n x = x : replicate' (n - 1) x

(!!!) :: [a] -> Int -> a
[]       !!! _ = error "???"
(x : _ ) !!! 0 = x
(_ : xs) !!! n = xs !!! (n - 1)


elem' :: Eq a => a -> [a] -> Bool
elem' _ [] = False
elem' t (x : xs) | x == t    = True
                 | otherwise = elem' t xs

main = do
  print $ and' [True, True, True]
  print $ and' [True, True, False]
  print $ and' [True, False, False]
  print $ and' [False, True]
  print $ and' [False, False]

  print (flatten [] :: [Int])
  print $ flatten [[1], [2], [3]]
  print $ flatten [[1, 2, 3]]
  print $ flatten [[1, 2], [3]]

  print $ replicate' 0 '*'
  print $ replicate' 1 '*'
  print $ replicate' 2 '*'
  print $ replicate' 3 '*'
  print $ replicate' 4 '*'

  print $ "ABCDEFG" !!! 0
  print $ "ABCDEFG" !!! 1
  print $ "ABCDEFG" !!! 4
  print $ "ABCDEFG" !!! 5
  -- print $ "ABCDEFG" !!! 10

  print $ elem' 'a' "ABCDEFG"
  print $ elem' 'A' "ABCDEFG"
  print $ elem' 'E' "ABCDEFG"
  print $ elem' 'X' "ABCDEFG"
