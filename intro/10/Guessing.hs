module Guessing
  () where


import           System.IO                      ( hSetEcho
                                                , stdin
                                                )


getChWithoutEcho :: IO Char
getChWithoutEcho = do
  hSetEcho stdin False
  x <- getChar
  hSetEcho stdin True
  return x

getSecret :: IO String
getSecret = do
  x <- getChWithoutEcho

  if x == '\n'
    then do
      putChar x
      return ""
    else do
      putChar '.'
      xs <- getSecret
      return (x : xs)


guess :: String -> IO ()
guess word = do
  putStr "? "
  attempt <- getLine

  if attempt == word
    then putStrLn "You got it!"
    else do
      putStrLn (showMatches word attempt)
      guess word


showMatches :: String -> String -> String
showMatches xs ys = [ if x `elem` ys then x else '-' | x <- xs ]


play :: IO ()
play = do
  putStr "Enter the secret word: "
  word <- getSecret

  putStrLn "Enter your guess:"
  guess word
