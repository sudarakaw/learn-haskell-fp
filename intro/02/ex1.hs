double x = x + x

quadruple x = double (double x)

factorial n = product [1 .. n]

average ns = sum ns `div` length ns

last1 xs = xs !! (length xs - 1)

last2 xs = head (reverse xs)

init1 xs = take (length xs - 1) xs

init2 xs = reverse (tail (reverse xs))
